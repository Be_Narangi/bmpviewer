﻿using System;

namespace BitmapProcessing
{
    public static class ImageOperation
    {
        public static Bitmap Negative(Bitmap bitmap)
        {
            return processPixelwise(bitmap, p =>
            {
                p.Red   = (byte)(byte.MaxValue - p.Red);
                p.Green = (byte)(byte.MaxValue - p.Green);
                p.Blue  = (byte)(byte.MaxValue - p.Blue);
            });
        }

        public static Bitmap Grayscale(Bitmap bitmap)
        {
            return processPixelwise(bitmap, p =>
            {
                byte gray = (byte)(p.Brightness);
                p.Decolorize(p, gray);
            });
        }

        public static Bitmap BlackAndWhite(Bitmap bitmap)
        {
            return processPixelwise(bitmap, p =>
            {
                byte bw = (p.Brightness >= 127) ? (byte)255 : (byte)0;
                p.Decolorize(p, bw);
            });
        }

        #region public static Bitmap Posterize

        public static Bitmap Posterize(Bitmap bitmap, int posterizeLevels)
        {
            return processPixelwise(bitmap, p =>
            {
                p.Red   = posterizeColor(p.Red, posterizeLevels);
                p.Green = posterizeColor(p.Green, posterizeLevels);
                p.Blue  = posterizeColor(p.Blue, posterizeLevels);
            });
        }

        static byte posterizeColor(byte color, int posterizeLevels)
        {
            double posterizeStep = 255 / posterizeLevels;
            int posterizeLevel = (int)(color / posterizeStep) + 1;
            double remainder = color / posterizeStep - (int)(color / posterizeStep);
            if (remainder < 0.5)
                return (byte)((posterizeLevel - 1) * (int)posterizeStep);
            else
                return (byte)(posterizeLevel * (int)posterizeStep);
        }

        #endregion

        public static Bitmap Dithering(Bitmap bitmap)
        {
            var random = new Random();
            return processPixelwise(bitmap, p =>
            {
                int r = random.Next(255);
                byte color = r < p.Brightness ? (byte)255 : (byte)0;
                p.Red   = color;
                p.Green = color;
                p.Blue  = color;
            });
        }

        public static Bitmap PhotoFilter(Bitmap bitmap, PixelColor filterColor, int blendLevel)
        {
            return processPixelwise(bitmap, p =>
            {
                p.Red   = p.Blend(p.Red, filterColor.Red, blendLevel);
                p.Green = p.Blend(p.Green, filterColor.Green, blendLevel);
                p.Blue  = p.Blend(p.Blue, filterColor.Blue, blendLevel);
            });
        }

        #region public static Bitmap GaussianBlur

        public static Bitmap GaussianBlur(Bitmap bitmap, double radius)
        {
            return bitmap;
        }

        static byte blurPixel(byte colorChanel, double radius)
        {
            byte outputPixel = 0;
            return outputPixel;
        }

        #endregion

        public static Bitmap ScaleDown(Bitmap bitmap, int scaleLevel)
        {
            var outputWidth  = Math.Max(bitmap.PixelArray.GetLength(0) / scaleLevel, 1);
            var outputHeight  = Math.Max(bitmap.PixelArray.GetLength(1) / scaleLevel, 1);

            var outputBitmap = new Bitmap(outputWidth, outputHeight);

            for (int i = 0; i < outputWidth; i ++)
                for (int j = 0; j < outputHeight; j ++)
                {
                    outputBitmap.PixelArray[i, j] = bitmap.PixelArray[i * scaleLevel, j * scaleLevel]; 
                }
            return outputBitmap;
        }

        static Bitmap processPixelwise(Bitmap bitmap, Action<PixelColor> processPixel)
        {
            var result = bitmap.Clone();
            var width  = bitmap.PixelArray.GetLength(0);
            var height = bitmap.PixelArray.GetLength(1);
            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y)
                {
                    processPixel(result.PixelArray[x, y]);
                }
            return result;
        }
    }
}
