﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitmapProcessing
{
    public class MathOperation
    {
        public double[,] NormalDistribution(double radius)
        {
            double mu = 0;
            double sigma = 1;

            int kernel;
            if ((int)radius % 2 == 0)
            {
                kernel = (int)radius + 1;
                sigma -= radius - (int)radius;
            }
            else
            {
                kernel = (int)radius;
                sigma += radius - (int)radius;
            }

            int i = 0, j = 0;
            double[,] nd = new double[kernel, kernel];
            double ndSum = 0;

            for (int x = -(kernel / 2); x <= kernel / 2; x++)
            {
                for (int y = -(kernel / 2); y < kernel / 2; y++)
                {
                    nd[i, j] = gaussianFunction(mu, sigma, x, y);
                    ndSum += nd[i, j];
                    j++;
                }
                i++;
            }

            return normalizedMatrix(nd, ndSum);
        }

        double gaussianFunction(double mu, double sigma, int x, int y)
        {
            return (1 / (sigma * Math.Sqrt(2 * Math.PI))) * Math.Pow(Math.E, 0 - Math.Pow((Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2)) - mu), 2) / (2 * Math.Pow(sigma, 2)));
        }

        double[,] normalizedMatrix(double[,] array, double arraySum)
        {
            if (arraySum != 1)
                for (int i = 0; i < array.GetLength(0); i++)
                    for (int j = 0; j < array.GetLength(1); j++)
                    {
                        array[i, j] *= 1 / arraySum;
                    }
            return array;
        }
    }
}
