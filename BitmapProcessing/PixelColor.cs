﻿using System;

namespace BitmapProcessing
{
    public class PixelColor
    {
        public PixelColor(byte red, byte green, byte blue)
        {
            Red   = red;
            Green = green;
            Blue  = blue;
            Brightness = (int)(0.2126 * Red + 0.7152 * Green + 0.0722 * Blue);
        }

        public byte Red   { get; set; }
        public byte Green { get; set; }
        public byte Blue  { get; set; }
        public int Brightness { get; set; }

        public byte Blend(byte colorChannel, byte filterChannel, int blendLevel)
        {
            double bl = blendLevel * 0.1;
            return (byte)((1 - bl) * colorChannel + bl * filterChannel);
        }

        public void Decolorize (PixelColor color, byte brightness)
        {
            color.Red   = brightness;
            color.Green = brightness;
            color.Blue  = brightness;
        }
    }
}
