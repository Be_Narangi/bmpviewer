﻿using System;
using System.Runtime.InteropServices;

namespace BitmapProcessing
{
    public class BITMAPFILEHEADER
    {
        public int Type;
        public int Size;
        public int OffBits;
    }
}
