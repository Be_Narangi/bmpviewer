﻿using System;
using System.Runtime.InteropServices;

namespace BitmapProcessing
{    
    public class BITMAPINFOHEADER
    {
        public int HeaderSize;
        public int Width;
        public int Height;
        public int Planes;
        public int BitCount;
        public int Compression;
        public int SizeImage => (Width * BitCount / 8 + Padding) * Height;
        public int XPelsPerMeter;
        public int YPelsPerMeter;
        public int ClrUsed;
        public int ClrImportant;

        public int Padding => (4 - Width * BitCount / 8 % 4) % 4;
    }
}
