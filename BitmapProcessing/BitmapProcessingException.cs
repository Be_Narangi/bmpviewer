﻿using System;

namespace BitmapProcessing
{
    [Serializable]
    public class BitmapProcessingException : Exception
    {
        public BitmapProcessingException() { }
        public BitmapProcessingException(string message) : base(message) { }
        public BitmapProcessingException(string message, Exception inner) : base(message, inner) { }
        protected BitmapProcessingException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
