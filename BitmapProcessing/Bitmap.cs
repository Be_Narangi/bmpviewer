﻿using System;
using System.IO;

namespace BitmapProcessing
{
    public class Bitmap
    {
        public Bitmap(string filename)
        {
            Load(filename);
        }

        public Bitmap(Stream stream)
        {
            Load(stream);
        }

        public Bitmap(int width, int height)
        {
            PixelArray = new PixelColor[width, height];
            PixelFormat = 24;
        }

        protected Bitmap(Bitmap bitmap)
        {
            PixelFormat = bitmap.PixelFormat;
            var width  = bitmap.PixelArray.GetLength(0);
            var height = bitmap.PixelArray.GetLength(1);

            PixelArray = new PixelColor[width, height];
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    var p = bitmap.PixelArray[i, j];
                    PixelArray[i, j] = new PixelColor(p.Red, p.Green, p.Blue);
                }
        }

        public Bitmap Clone()
        {
            return new Bitmap(this);
        }

        public void Load(string filename)
        {
            using (var filestream = new FileStream(filename, FileMode.Open))
            {
                Load(filestream);
            }
        }

        public void Load(Stream stream)
        {
            using (var br = new BinaryReader(stream))
            {
                var bmpFileHeader = getBMPFileHeader(br);
                var bmpInfoHeader = getBMPInfoHeader(br);
                PixelArray = getPixelArray(stream, bmpFileHeader, bmpInfoHeader);
            }
        }

        public int PixelFormat { get; protected set; }
        public PixelColor[,] PixelArray { get; protected set; }

        PixelColor[,] getPixelArray(Stream bmpstream, BITMAPFILEHEADER bmpFileHeader, BITMAPINFOHEADER bmpInfoHeader)
        {
            var byteArray = getByteArray(bmpstream, bmpFileHeader, bmpInfoHeader);

            var array = new PixelColor[bmpInfoHeader.Width, bmpInfoHeader.Height];
            int arrayWidth  = array.GetLength(0);
            int arrayHeight = array.GetLength(1);

            int byteArrayIndex = 0;


            for (int colIndex = arrayHeight - 1; colIndex >= 0; colIndex--)
            {
                for (int rowIndex = 0; rowIndex < arrayWidth; ++rowIndex)
                {
                    array[rowIndex, colIndex] = new PixelColor(byteArray[byteArrayIndex + 2],
                                                               byteArray[byteArrayIndex + 1],
                                                               byteArray[byteArrayIndex + 0]);
                    byteArrayIndex += 3;
                }

                byteArrayIndex += bmpInfoHeader.Padding;
            }

            return array;
        }

        byte[] getByteArray(Stream bmpstream, BITMAPFILEHEADER bmpFileHeader, BITMAPINFOHEADER bmpInfoHeader)
        {
            int arrayLength = bmpInfoHeader.SizeImage;
            byte[] array = new byte[arrayLength];
            bmpstream.Seek(bmpFileHeader.OffBits, SeekOrigin.Begin);

            bmpstream.Read(array, 0, arrayLength);

            return array;
        }

        static BITMAPFILEHEADER getBMPFileHeader (BinaryReader br)
        {
            var header = new BITMAPFILEHEADER();

            if (br.ReadInt16() != ((77 << 8) + 66))
                throw new BitmapProcessingException("Неверный тип файла.");

            br.ReadBytes(8); // Skip Size
            header.OffBits = br.ReadInt32();
            return header;
        }

        static BITMAPINFOHEADER getBMPInfoHeader (BinaryReader br)
        {
            var header = new BITMAPINFOHEADER();

            br.ReadInt32(); // Skip HeaderSize

            header.Width = br.ReadInt32();
            header.Height = br.ReadInt32();

            br.ReadInt16();  // Skip Planes

            header.BitCount = br.ReadUInt16();
            if (header.BitCount != 24)
                throw new BitmapProcessingException("Поддерживаются только 24-битные изображения.");
            
            return header;
        }
    }
}
