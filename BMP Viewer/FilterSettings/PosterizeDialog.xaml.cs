﻿using System;
using System.Windows;
using XamlGeneratedNamespace;

namespace BMP_Viewer.FilterSettings
{   
    public partial class PosterizeDialog : Window
    {
        public int PosterizeLevels { get; set; }

        public PosterizeDialog()
        {
            PosterizeLevels = 2;
            InitializeComponent();
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
