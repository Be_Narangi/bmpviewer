﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace BMP_Viewer.FilterSettings
{
    public partial class PhotoFilterDialog : Window
    {
        public Color SelectedColor => m_colorPicker.SelectedColor.Value;
        public int BlendLevel => (int)m_blendLevelSlider.Value;

        public PhotoFilterDialog()
        {
            InitializeComponent();
            setAvailableColors();
        }

        private void setAvailableColors()
        {
            var colors = new ObservableCollection<ColorItem>
            {
                new ColorItem(Colors.Red,     "Red"    ),
                new ColorItem(Colors.Orange,  "Orange" ),
                new ColorItem(Colors.Yellow,  "Yellow" ),
                new ColorItem(Colors.Green,   "Green"  ),
                new ColorItem(Colors.Cyan,    "Cyan"   ),
                new ColorItem(Colors.Blue,    "Blue"   ),
                new ColorItem(Colors.Violet,  "Violet" ),
                new ColorItem(Colors.Magenta, "Magenta")
            };

            m_colorPicker.AvailableColors = colors;
            m_colorPicker.SelectedColor = colors.FirstOrDefault()?.Color ?? Colors.Magenta;
        }

        private void OK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
