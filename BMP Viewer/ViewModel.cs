﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;

using BitmapProcessing;
using System.Windows.Media;

namespace BMP_Viewer
{
    public class ViewModel : INotifyPropertyChanged
    {
        public WriteableBitmap InitialBitmap { get; set; }
        public WriteableBitmap ResultingBitmap { get; set; }

        public void LoadImage(string filename)
        {
            bitmap = new Bitmap(filename);
            InitialBitmap = makeWritableBitmap(bitmap);
        }

        Bitmap bitmap;

        static WriteableBitmap makeWritableBitmap(Bitmap bitmap)
        {
            var width  = bitmap.PixelArray.GetLength(0);
            var height = bitmap.PixelArray.GetLength(1);
            WriteableBitmap image = BitmapFactory.New(width, height);
            for (int x = 0; x < width; ++x)
                for (int y = 0; y < height; ++y)
                {
                    var p = bitmap.PixelArray[x, y];
                    image.SetPixel(x, y, Color.FromRgb(p.Red, p.Green, p.Blue));
                }

            return image;
        }

        public void RenderNegative()
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.Negative(bitmap));
        }

        public void RenderGrayscale()
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.Grayscale(bitmap));
        }

        public void RenderBlackAndWhite()
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.BlackAndWhite(bitmap));
        }

        public void RenderPosterize(int posterizeLevels)
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.Posterize(bitmap, posterizeLevels - 1));
        }

        public void RenderDithering()
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.Dithering(bitmap));
        }

        public void RenderPhotoFilter(Color? color, int blendLevel)
        {
            PixelColor filterColor = new PixelColor(color.Value.R, color.Value.G, color.Value.B);
            ResultingBitmap = makeWritableBitmap(ImageOperation.PhotoFilter(bitmap, filterColor, blendLevel));
        }

        public void RenderGaussianBlur(double blurRadius)
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.GaussianBlur(bitmap, blurRadius));
        }

        public void RenderScaleDown(int scaleLevel)
        {
            ResultingBitmap = makeWritableBitmap(ImageOperation.ScaleDown(bitmap, scaleLevel));
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }        
        #endregion
    }
}
