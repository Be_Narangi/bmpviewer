﻿using Microsoft.Win32;
using System;
using System.Windows;

using BMP_Viewer.FilterSettings;
using BMP_Viewer.ScaleSettings;

namespace BMP_Viewer
{
    public partial class MainWindow : Window
    {
        string path { get; set; }
        public ViewModel ViewModel { get; } = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void open_File_Click(object sender, RoutedEventArgs e)
        {
            while (true)
            {
                try
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "Image files (*.bmp)|*.bmp";
                    if (ofd.ShowDialog() == true)
                        ViewModel.LoadImage(ofd.FileName);

                    break;
                }
                catch (Exception exc)
                {
                    MessageBox.Show(exc.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void Negative_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RenderNegative();
        }

        private void Grayscale_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RenderGrayscale();
        }

        private void BlackAndWhite_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RenderBlackAndWhite();
        }

        private void Posterize_Click(object sender, RoutedEventArgs e)
        {
            var posterize = new PosterizeDialog();
            bool? result = posterize.ShowDialog();
            if (result.HasValue && result.Value)
            {
                ViewModel.RenderPosterize(posterize.PosterizeLevels);
            }
        }

        private void Dithering_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RenderDithering();
        }

        private void PhotoFilter_Click(object sender, RoutedEventArgs e)
        {
            var photoFilter = new PhotoFilterDialog();
            bool? result = photoFilter.ShowDialog();
            if (result.HasValue && result.Value)
            {
                ViewModel.RenderPhotoFilter(photoFilter.SelectedColor, photoFilter.BlendLevel);
            }
        }

        private void GaussianBlur_Click(object sender, RoutedEventArgs e)
        {
            var gaussianBlur = new GaussianBlurDialog();
            bool? result = gaussianBlur.ShowDialog();
            if (result.HasValue && result.Value)
            {
                ViewModel.RenderGaussianBlur(gaussianBlur.BlurRadius);
            }
        }

        private void ScaleDown_Click(object sender, RoutedEventArgs e)
        {
            var scaleDown = new ScaleDownDialog();
            bool? result = scaleDown.ShowDialog();
            if (result.HasValue && result.Value)
                ViewModel.RenderScaleDown(scaleDown.Level);
        }
    }
}
